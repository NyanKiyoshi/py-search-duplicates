Py Search Duplicates [1.1]
===========================================
A Python2 script to search all duplicates files into a given directory recursively. It hash each file with the MD5 message-digest algorithm to compare each file and find who already exist.

Table of content:
-----------------
1. [Introduction](http://nyankiyoshi.github.io/docs/py-search-duplicates/1.1/index.html)
2. [Requirements and Downloads](http://nyankiyoshi.github.io/docs/py-search-duplicates/1.1/download.html)
4. [Usage](http://nyankiyoshi.github.io/docs/py-search-duplicates/1.1/usage.html)
4. [ChangeLog](http://nyankiyoshi.github.io/docs/py-search-duplicates/changelog.html#1.1)

Documentation:
--------------
[Available here](http://nyankiyoshi.github.io/docs/py-search-duplicates/1.1/)

Miscellaneous information:
--------------------------
This Python script was developed on Python 2.7.8 on Linux-2 with ArchLinux.

If you have any suggestion, improvement, issue or others don't hesitate to ask or make a pull request! We will be delighted to read it and response you!
